/* ----------------------------------------------------------------------------
URL: http://bitbucket.org/mortar/breakpoints
Author: Daniel Charman (daniel@blackmaze.com.au)
Created: 2014-05-24
---------------------------------------------------------------------------- */
(function( $ ) {
 
    $.breakpoints = function(options) {

        var defaults = {
            debug:      false,
            xsmall:     '380',
            small:      '600',
            medium:     '768', 
            large:      '1024',
            xlarge:     '1280'
        };
     
        var settings = $.extend( {}, defaults, options );

        var self = this;

        //get viewport dimensions
        self.width = $(window).width();
        self.height = $(window).height();

        //detect orientation
        self.orientation = (self.width > self.height) ? 'landscape' : 'portrait';

        //detect breakpoint sizes
        self.size = 'xsmall';
        // if(self.width <= settings.xsmall) self.size = 'xsmall';
        if(self.width > settings.xsmall && self.width <= settings.small) self.size = 'small';
        if(self.width > settings.small && self.width <= settings.medium) self.size = 'medium';
        if(self.width > settings.medium && self.width <= settings.large) self.size = 'large';
        if(self.width > settings.large) self.size = 'xlarge';

        self.render = function() {
            //make sure element doesnt exist before creating it
            if(!$('body').find('.m-breakpoint').length) {
                $('body').append('<div class="m-breakpoint"></div>');
            }

            //set/update size data
            $('body').find('.m-breakpoint').html(
                self.size + ' (' + (self.width + 'x' + self.height) + ')'
            );
        };

        if(settings.debug) {
            self.render();
        }

        return self;
    };

}( jQuery ));